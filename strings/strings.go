package strings

import (
	"strings"
	"unicode"
)

// Capitalize receive a string and turns its first character to UpperCase
func Capitalize(s string) string {
	if s == "" {
		return ""
	}

	rs := []rune(s)
	rs[0] = unicode.ToTitle(rs[0])

	return string(rs)
}

// SnakeCaseToCamelCase receives a string underscore separated and turns a UpperCase separated string (CamelCase)
func SnakeCaseToCamelCase(s string) string {
	parts := strings.Split(s, "_")
	camelCaseStr := ""
	for _, part := range parts {
		camelCaseStr += Capitalize(strings.ToLower(part))
	}
	return camelCaseStr
}
